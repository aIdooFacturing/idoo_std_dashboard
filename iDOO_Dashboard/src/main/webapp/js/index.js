const loadPage = () =>{
	createMenuTree("monitoring", "dashboard")
	
	$("#svg").css({
    	"position" : "relative",
    	
    	"width" : $("#container").width(),
    	"height" : $("#container").height()
    	
    });
	// 7초마다 다시 그리기
	setInterval(function(){
		createMachine();
	}, 7000);

	// 60초마다 공지사항 변경 확인하기
	setInterval(function(){
		chkBanner();
	}, 60000);
	// 5분 마다 페이지 새로고침
	setInterval(function(){
		location.reload()
	}, 300000);
	bindMyEvt()
	createStatusLabel()
	createMachine()
	
	setEl();

//	chkBanner();

}


let dateInterval = null;

const drawGroupDiv = () =>{
	//LFA RR
	var table = "<table class='gr_table' id='lfa_rr'>" + 
					"<tr>" + 
						"<Td class='label'>MC ED / FFM<br>VF LS</td>" +
					"</tr>" + 
					"<tr>" + 
						"<Td class='icon'></td>" +
					"</tr>" +
				"</table>";

	$("#container").append(table);
			
	$("#lfa_rr").css({
		"left" : getElSize(265) + marginWidth,
		"top" : getElSize(380 - 20) + marginHeight,	
	});
	
	$("#lfa_rr").css({
		"width" : getElSize(190)
	});
	
	//YP FRT (cnc)
	var table = "<table class='gr_table' id='yp_frt'>" + 
					"<tr>" + 
						//"<Td class='label'>YP FRT (CNC)<br>내수, 북미</td>" +
						"<Td class='label'>EN ACF<br>AC WLV 14 / 11</td>" +
					"</tr>" + 
					"<tr>" + 
						"<Td class='icon'></td>" + 
					"</tr>" +
				"</table>";

	$("#container").append(table);
			
	$("#yp_frt").css({
		"left" : getElSize(730 - 235) + marginWidth,
		"top" : getElSize(380 - 20) + marginHeight,	
	});
	
	$("#yp_frt td").css({
		"width" : getElSize(250)
	});

	//UM FRT (CNC)
	var table = "<table class='gr_table' id='um_frt'>" + 
					"<tr>" + 
						"<Td class='label'>CC RKF 16 / QV BG /<br>EF LL 16</td>" + 
					"</tr>" + 
					"<tr>" + 
						"<Td class='icon'></td>" + 
					"</tr>" +
				"</table>";

	$("#container").append(table);
			
	$("#um_frt").css({
		"left" : getElSize(1030 - 235) + marginWidth,
		"top" : getElSize(380 - 20) + marginHeight,	
	});
	
	$("#um_frt td").css({
		"width" : getElSize(400)
	});
	
	//TA RR
	var table = "<table class='gr_table' id='ta_Rr'>" + 
					"<tr>" + 
						"<Td class='label'>QMF VM /<br>AA WOL</td>" + 
					"</tr>" + 
					"<tr>" + 
						"<Td class='icon'></td>" + 
					"</tr>" +
				"</table>";

	$("#container").append(table);
			
	$("#ta_Rr").css({
		"left" : getElSize(1490 - 235) + marginWidth,
		"top" : getElSize(380 - 20) + marginHeight,	
	});
	
	$("#ta_Rr td").css({
		"width" : getElSize(250)
	});
	
	//TA FRT
	var table = "<table class='gr_table' id='ta_frt'>" + 
					"<tr>" + 
						"<Td class='label'>SM QKF /<br>VL NQO</td>" + 
					"</tr>" + 
					"<tr>" + 
						"<Td class='icon'></td>" + 
					"</tr>" +
				"</table>";

	$("#container").append(table);
			
	$("#ta_frt").css({
		"left" : getElSize(1800 - 235) + marginWidth,
		"top" : getElSize(380 - 20) + marginHeight,	
	});
	
	$("#ta_frt td").css({
		"width" : getElSize(250)
	});
	
	
	//JC RR
	var table = "<table class='gr_table' id='jc_rr'>" + 
					"<tr>" + 
						"<Td class='label'>LQ LLX /<br>MF RKG</td>" + 
					"</tr>" + 
					"<tr>" + 
						"<Td class='icon'></td>" + 
					"</tr>" +
				"</table>";

	$("#container").append(table);
			
	$("#jc_rr").css({
		"left" : getElSize(2100 - 235) + marginWidth,
		"top" : getElSize(380 - 20) + marginHeight,	
	});
	
	$("#jc_rr td").css({
		"width" : getElSize(250)
	});
	
	//
	var table = "<table class='gr_table' id='jc_rr2'>" + 
					"<tr>" + 
						"<Td class='label'>VL QQ 자동</td>" + 
					"</tr>" + 
					"<tr>" + 
						"<Td class='icon'></td>" + 
					"</tr>" +
				"</table>";

	$("#container").append(table);
			
	$("#jc_rr2").css({
		"left" : getElSize(2400 - 235) + marginWidth,
		"top" : getElSize(380 - 20) + marginHeight,	
	});
	
	$("#jc_rr2 td").css({
		"width" : getElSize(250)
	});
	
	
	//HR PU/FRT
	var table = "<table class='gr_table' id='hr_pu'>" + 
					"<tr>" + 
						"<Td class='label'>CM ED</td>" + 
					"</tr>" + 
					"<tr>" + 
						"<Td class='icon'></td>" + 
					"</tr>" +
				"</table>";

	$("#container").append(table);
			
	$("#hr_pu").css({
		"left" : getElSize(2700 - 235) + marginWidth,
		"top" : getElSize(380 - 20) + marginHeight,	
	});
	
	$("#hr_pu td").css({
		"width" : getElSize(300)
	});
	
	
	//HR PU/FRT
	var table = "<table class='gr_table' id='hr_pu2'>" + 
					"<tr>" + 
						"<Td class='label'>QE KRM 13</td>" + 
					"</tr>" + 
					"<tr>" + 
						"<Td class='icon'></td>" + 
					"</tr>" +
				"</table>";

	$("#container").append(table);
			
	$("#hr_pu2").css({
		"left" : getElSize(3040 - 235) + marginWidth,
		"top" : getElSize(380 - 20) + marginHeight,	
	});
	
	$("#hr_pu2 td").css({
		"width" : getElSize(250)
	});
	
	//HR PU/FRT
	var table = "<table class='gr_table' id='hr_pu3'>" + 
					"<tr>" + 
						"<Td class='label'>MS EKF</td>" + 
					"</tr>" + 
					"<tr>" + 
						"<Td class='icon'></td>" + 
					"</tr>" +
				"</table>";

	$("#container").append(table);
			
	$("#hr_pu3").css({
		"left" : getElSize(3340 - 235) + marginWidth,
		"top" : getElSize(380 - 20) + marginHeight,	
	});
	
	$("#hr_pu3 td").css({
		"width" : getElSize(250)
	});
	
	
	//TQ FRT
	var table = "<table class='gr_table' id='tq_frt'>" + 
					"<tr>" + 
						"<Td class='label'>EV VFV /<br>RF BM</td>" + 
					"</tr>" + 
					"<tr>" + 
						"<Td class='icon'></td>" + 
					"</tr>" +
				"</table>";

	$("#container").append(table);
			
	$("#tq_frt").css({
		"left" : getElSize(3640 - 235) + marginWidth,
		"top" : getElSize(380 - 20) + marginHeight,	
	});
	
	$("#tq_frt td").css({
		"width" : getElSize(190)
	});
	
	
	
	
	
	
	var table = "<table class='gr_table' id='yp_rr'>" + 
						"<tr>" + 
							"<Td class='label'>RLF 26 / VMV 55</td>" + 
						"</tr>" + 
						"<tr>" + 
							"<Td class='icon'></td>" + 
						"</tr>" +
					"</table>";
				
				$("#container").append(table);
				
				$("#yp_rr").css({
					"left" : getElSize(2330  - 235) + marginWidth,
					"top" : getElSize(1180 - 10) + marginHeight,	
				});
				
				$("#yp_rr td").css({
					"width" : getElSize(470)
				});

	var table = "<table class='gr_table' id='yp_rr2'>" + 
				"<tr>" + 
					"<Td class='label'>RM SK LV/<br>RM CD RF</td>" + 
				"</tr>" + 
				"<tr>" + 
					"<Td class='icon'></td>" + 
				"</tr>" +
			"</table>";
		
		$("#container").append(table);
		
		$("#yp_rr2").css({
			"left" : getElSize(2850 - 235) + marginWidth,
			"top" : getElSize(1180 - 10) + marginHeight,	
		});
		
		$("#yp_rr2 td").css({
			"width" : getElSize(150)
		});
		
		
		var table = "<table class='gr_table' id='yp_rr3'>" + 
				"<tr>" + 
					"<Td class='label'>EMF LV</td>" + 
				"</tr>" + 
				"<tr>" + 
					"<Td class='icon'></td>" + 
				"</tr>" +
			"</table>";
		
		$("#container").append(table);
		
		$("#yp_rr3").css({
			"left" : getElSize(3040 - 235) + marginWidth,
			"top" : getElSize(1180 - 10) + marginHeight,	
		});
		
		$("#yp_rr3 td").css({
			"width" : getElSize(250)
		});
	
		
		var table = "<table class='gr_table' id='yp_rr4'>" + 
				"<tr>" + 
					"<Td class='label'>CM QD / LV LCQ</td>" + 
				"</tr>" + 
				"<tr>" + 
					"<Td class='icon'></td>" + 
				"</tr>" +
			"</table>";
		
		$("#container").append(table);
		
		$("#yp_rr4").css({
			"left" : getElSize(3340 - 235) + marginWidth,
			"top" : getElSize(1180 - 10) + marginHeight,	
		});
		
		$("#yp_rr4 td").css({
			"width" : getElSize(250)
		});
		
		
		//UM FRT
		var table = "<table class='gr_table' id='yp_rr5'>" + 
		"<tr>" + 
			"<Td class='label'>FM QEK /<br>CV OKL</td>" + 
		"</tr>" + 
		"<tr>" + 
			"<Td class='icon'></td>" + 
		"</tr>" +
		"</table>";
	
		$("#container").append(table);
		
		$("#yp_rr5").css({
			"left" : getElSize(3640 - 235) + marginWidth,
			"top" : getElSize(1180 - 10) + marginHeight,	
		});
		
		$("#yp_rr5 td").css({
			"width" : getElSize(190)
		});
		
	
	var table = "<table class='gr_table' id='tmp'>" + 
			"<tr>" + 
				"<Td class='label'>SAMPLE</td>" + 
			"</tr>" + 
			"<tr>" + 
				"<Td class='icon'></td>" +
			"</tr>" +
		"</table>";
	
	$("#container").append(table);
	
	$("#tmp").css({
		"left" : getElSize(500 - 235) + marginWidth,
		"top" : getElSize(1180 - 10) + marginHeight,	
	});
	
	

	
	$(".gr_table").css({
		"position" : "absolute",
		"border-spacing" : "0px",
		//"border-collapse" : "collapse",
		"z-index" : 1
	});
	
	
	
	
	$(".gr_table .label").css({
		"background-color" : "#373737",
		"text-align" : "center",
		"height" : getElSize(70),
		//"border" : getElSize(3) + "px solid #535556",
		"border-top-left-radius" : getElSize(4 * 2) + "px",
		"border-top-right-radius" : getElSize(4 * 2) + "px",
	});
	
	$(".gr_table .icon").css({
		//"height" : getElSize(530),
		"height" : getElSize(730),
		"border-left" : getElSize(3) + "px solid #535556",
		"border-right" : getElSize(3) + "px solid #535556",
		"border-bottom" : getElSize(3) + "px solid #535556",
		"border-bottom-left-radius" : getElSize(4 * 2) + "px",
		"border-bottom-right-radius" : getElSize(4 * 2) + "px",

	});
	
	$("#tmp .icon").css({
		"width" : getElSize(190),
		"height" : getElSize(630)
	});
	
	$("#lfa_rr .icon,#yp_frt .icon,  #ta_frt .icon, #jc_rr .icon, #hr_pu .icon").css({
		"height" : getElSize(730),

	});
	
	$("#tq_frt .icon").css({
		"height" : getElSize(1700),

	});
	
	$("#yp_rr, #yp_rr2, #yp_rr3, #yp_rr4, #yp_rr5").css({
		"height" : getElSize(980),
	})
	
	$("#fs_rr .icon").css({
		"height" : getElSize(790),

	});
	
	$(".gr_table td").css({
		"color" : "white",
		"font-size" : getElSize(30),
	});
	
	
};


const createMachine = () =>{
	const url = ctxPath + "/svg/getMachineInfo.do";
	const date = new Date();
	
	const year = date.getFullYear();
	const month = addZero(String(date.getMonth()+1));
	const day = addZero(String(date.getDate()));
	const today = year + "-" + month + "-" + day;
	
	const param = "shopId=" + shopId + 
				"&startDateTime=" + today;
	
	$.ajax({
		url : url,
		data : param,
		type : "post",
		dataType : "json",
		success : (data) => {
			console.log("reset")
			let boxes = "";
			
			let incycleCnt = waitCnt = alarmCnt = noConnCnt = 0;
			
			for (var json of data.machineList){
				let bgColor = "";
				if(json.lastChartStatus == "IN-CYCLE"){
					bgColor = incycleColor;
					incycleCnt++
				}else if(json.lastChartStatus == "WAIT"){
					bgColor = waitColor;
					waitCnt++
				}else if(json.lastChartStatus == "ALARM"){
					bgColor = alarmColor;
					alarmCnt++
				}else if(json.lastChartStatus == "NO-CONNECTION" && json.notUse != 1) {
					bgColor = noConnColor;
					noConnCnt++
				}
				
				const name = decodeURIComponent(json.name)
				
				var array_name = name.split("");
				
				var realName = "";
				var strSize = 0;
				$(array_name).each(function(idx,data){
					strSize += 27;
					if(json.w < strSize || data==" "){
						realName += "<br>";
						strSize = 0;
					}
					realName += data;
				});
				
				realName += "<br>(" + decode(json.ty) + ")";
				
				if(json.id != 97){
					boxes += 
						`
							<div class="machine"
								dvcId="${json.id}"
								dvcName="${json.name}"
								status="${json.lastChartStatus}"
								style= 
								"	position : absolute
									; z-index : 2
									; background-color : ${bgColor}
									; width : ${getElSize(json.w)}px
									; height : ${getElSize(json.h)}px
									; border-radius : ${getElSize(4 * 2)}px
									; left : ${getElSize(json.x)}px
									; top : ${getElSize(json.y)}px
									; font-size : ${getElSize(json.fontSize)}px
									; text-align : center
									; cursor : pointer
									; transition : 0.5s
								"
							>${realName}</div>
							
						`	
				}
				
				//console.log(json)
			}
			
			$("#incycleCnt").html(incycleCnt)
			$("#waitCnt").html(waitCnt)
			$("#alarmCnt").html(alarmCnt)
			$("#noConnCnt").html(noConnCnt)
			
			$("#totalCnt").html((incycleCnt + waitCnt + alarmCnt + noConnCnt))
			
			$("#svg").append(boxes)
			
			$(".machine").hover((el)=>{
				$(el.currentTarget).css({
					"background-color" : "white"
				})
			}, (el)=>{
				let status = $(el.currentTarget).attr("status")
				let bgColor = ""
					
				if(status == "IN-CYCLE"){
					bgColor = incycleColor;
				}else if(status == "WAIT"){
					bgColor = waitColor;
				}else if(status == "ALARM"){
					bgColor = alarmColor;
				}else if(status == "NO-CONNECTION"){
					bgColor = noConnColor;
				}
				
				$(el.currentTarget).css({
					"background-color" : bgColor
				})
			}).click((el) =>{
				const id = $(el.currentTarget).attr("dvcId")
				const name = $(el.currentTarget).attr("dvcName")
				
				window.localStorage.setItem("dvcId", id);
				window.localStorage.setItem("name", name);
				
				//location.href=ctxPath + "/chart/singleChartStatus.do?fromDashBoard=true";
			
				let url = "";
				if(appTy == "auto"){
					url = ""
				}else{
					url = "_STD"
				}
				location.href = `/iDOO${url}_Single_Chart_Status/index.do?fromDashBoard=true&lang=${lang}`
				
				console.log(id)
			})
		}, error : (e1,e2,e3) =>{
			console.log(e1,e2,e3)
		}
	});
};

const createStatusLabel = () =>{
	const img = 
		`
			<img src="${ctxPath}/images/FL/layout_view_info.svg"
				style = 
				"
					position : absolute
					; z-index : 2
					; height : ${getElSize(75 * 2)}px
					; top : ${getElSize(939 * 2) + marginHeight}px
					; left : ${getElSize(48 * 2) + marginWidth}px
				"
			>		
		`
		
	$("#container").append(img)
	
	const cutting_title = 
		`
			<span
				style=
				"
					font-size : ${getElSize(12 * 2)}px
					; color : white
					; position : absolute
					; z-index : 2
					; top : ${getElSize(1014 * 2) + marginHeight}px
					; left : ${getElSize(50 * 2) + marginWidth}px
				"
			>CUTTING</span>
		`
		
	const wait_title = 
		`
			<span
				style=
				"
					font-size : ${getElSize(12 * 2)}px
					; color : white
					; position : absolute
					; z-index : 2
					; top : ${getElSize(1014 * 2) + marginHeight}px
					; left : ${getElSize(116 * 2) + marginWidth}px
				"
			>WAITING</span>
		`
			
	const alarm_title = 
		`
			<span
				style=
				"
					font-size : ${getElSize(12 * 2)}px
					; color : white
					; position : absolute
					; z-index : 2
					; top : ${getElSize(1014 * 2) + marginHeight}px
					; left : ${getElSize(185 * 2) + marginWidth}px
				"
			>ALARM</span>
		`
				
	const noConn_title = 
		`
		<span
			style=
			"
				font-size : ${getElSize(12 * 2)}px
				; color : white
				; position : absolute
				; z-index : 2
				; top : ${getElSize(1014 * 2) + marginHeight}px
				; left : ${getElSize(257 * 2) + marginWidth}px
			"
		>OFF</span>
	`
	const total_title = 
		`
		<span
			style=
			"
				font-size : ${getElSize(12 * 2)}px
				; color : white
				; position : absolute
				; z-index : 2
				; top : ${getElSize(1014 * 2) + marginHeight}px
				; left : ${getElSize(313.5 * 2) + marginWidth}px
			"
		>TOTAL</span>
	`		
	$("#container").append(cutting_title, wait_title, alarm_title, noConn_title, total_title)
	
	const incycleVal = 
		`
			<span id="incycleCnt"
				style=
				"
					font-size : ${getElSize(32 * 2)}px
					; color : #AED543
					; position : absolute
					; z-index : 2
					; top : ${getElSize(967 * 2) + marginHeight}px
					; left : ${getElSize(60 * 2) + marginWidth}px
				"
			></span>
		`
	const waitVal = 
		`
			<span id="waitCnt"
				style=
				"
					font-size : ${getElSize(32 * 2)}px
					; color : #F19537
					; position : absolute
					; z-index : 2
					; top : ${getElSize(967 * 2) + marginHeight}px
					; left : ${getElSize(123 * 2) + marginWidth}px
				"
			></span>
		`	
		
	const alarmVal = 
		`
			<span id="alarmCnt"
				style=
				"
					font-size : ${getElSize(32 * 2)}px
					; color : #B42F1A
					; position : absolute
					; z-index : 2
					; top : ${getElSize(967 * 2) + marginHeight}px
					; left : ${getElSize(194 * 2) + marginWidth}px
				"
			></span>
		`
		
	const noConnVal = 
		`
			<span id="noConnCnt"
				style=
				"
					font-size : ${getElSize(32 * 2)}px
					; color : #AEAEAF
					; position : absolute
					; z-index : 2
					; top : ${getElSize(967 * 2) + marginHeight}px
					; left : ${getElSize(250 * 2) + marginWidth}px
				"
			></span>
		`
		
	const totalVal = 
		`
			<span id="totalCnt"
				style=
				"
					font-size : ${getElSize(32 * 2)}px
					; color : #ffffff
					; position : absolute
					; z-index : 2
					; top : ${getElSize(967 * 2) + marginHeight}px
					; left : ${getElSize(305 * 2) + marginWidth}px
				"
			></span>
		`
	$("#container").append(incycleVal, waitVal, alarmVal, noConnVal, totalVal)

}

const setEl = () =>{
	$("#layout").css({
		"top" : 0,
		"left" : getElSize(20),
		"height" : getElSize(1410),
		"width" : getElSize(3125)
	})
	
	$("#intro").css({
		"position" : "absolute",
		"bottom" : 0 + marginHeight,
		"font-size" : getElSize(140),
		"font-weight" : "bolder",
		"z-index" : 9999
	});
	
	$("#intro_back").css({
		"width" : originWidth,
		"display" : "none",
		"height" : getElSize(180),
		"opacity" : 0.5,
		"position" : "absolute",
		"background-color" : "black",
		"bottom" : 0 + marginHeight,
		"z-index" : 9999,
	})

	$("#intro").css({
		"font-size" : getElSize(100)
	});
};
	
const bindMyEvt = () =>{
	
}

