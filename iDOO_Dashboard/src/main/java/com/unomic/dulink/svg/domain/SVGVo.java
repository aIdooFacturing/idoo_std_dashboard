package com.unomic.dulink.svg.domain;

import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

@Getter
@Setter
@ToString
public class SVGVo{
	int id;
	String name;
	String opRatio;
	int x;
	int y;
	int w;
	int h;
	float width;
	String workDate;
	String tgCyl;
	String cntCyl;
	String worker;
	String nd;
	String ieX;
	String chgTy;
	String type;
	String ieY;
	float height;
	String viewBox;
	String d;
	String transform;
	String isChg;
	String pic;
	int notUse;
	String status;
	String lastChartStatus;
	double spd_load;
	double feed_override;
	String alarm;
	String endDateTime;
	String startDateTime;
	Integer shopId;
	int adt_id;
	int m_id;
	int dvcId;
	int adapter_id;
	String operationTime;
	int fontSize;
	int red;
	int redBlink;
	int yellow;
	int yellowBlink;
	int green;
	int greenBlink;
};
